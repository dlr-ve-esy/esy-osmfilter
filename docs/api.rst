API 
===


.. automodule:: esy.osmfilter.osm_filter
    :members:



.. automodule:: esy.osmfilter.element_filter
    :members:


.. automodule:: esy.osmfilter.osm_pickle
    :members: 



.. automodule:: esy.osmfilter.pre_filter
    :members: 



.. automodule:: esy.osmfilter.osm_info
    :members: 



.. automodule:: esy.osmfilter.osm_colors
    :members: 


