``` eval_rst

esy-osmfilter
=============



`esy-osmfilter` is a Python library to read and filter `OpenStreetMap <https://www.openstreetmap.org>`_ data files in the `Protocol
Buffers (PBF) <https://developers.google.com/protocol-buffers/>`_ format and export them to a Python dictionary and/or JSON file.

The `OpenStreetMap PBF format <https://wiki.openstreetmap.org/wiki/PBF_Format>`_
defines three primary data types:

- ``Node``: An annotated point on Earth
- ``Way``: An list of ``Node`` items forming a path or polygon
- ``Relation``: A set of related entries

This library relies on the esy.osm.pbf library in order to read the pbf-files.

Features
--------

What it provides:

- A pythonic way to work with ``.pbf`` data file entries
- An efficent way to filter OpenStreetMap data with Python
- customisable filters 
- improved preformance by 
  - parallization of the filtering process
  - storage and reuse of (intermediate) results with PICKLE and JSON

What it **doesn't** provide:

- A mechanism to spatially query OpenStreetMap entries.
- Visualization of OpenStreetMap data.


Installation
------------

``esy-osmfilter`` depends on a Python version of 3.5 or above as well as on the
`Google Protocol Buffers <https://pypi.org/project/protobuf/>`_. Use ``pip`` to
install ``esy-osmfilter``:

.. code:: bash

    $ pip install esy-osmfilter


License
-------

``esy-osmfilter`` is licensed under the `GNU General Public License version
3.0 <https://www.gnu.org/licenses/gpl-3.0.html>`_.


The Team
--------

``esy-osmfilter`` is developed at the
`DLR <https://www.dlr.de/EN/Home/home_node.html>`_ Institute of
`Networked Energy Systems
<https://www.dlr.de/ve/en/desktopdefault.aspx/tabid-12472/21440_read-49440/>`_
in the departement for `Energy Systems Analysis (ESY)
<https://www.dlr.de/ve/en/desktopdefault.aspx/tabid-12471/21741_read-49802/>`_.
```
